package common;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ClientApplication implements CommandLineRunner {

    public static void main(String args[]) {
        SpringApplication.run(ClientApplication.class);
    }

    @Override
    public void run(String... strings) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String[] quotes = new String[]{"Dear God, they’ll be killed on our doorstep! And there’s no trash pickup until January 3rd.",
                "Your music’s bad and you should feel bad!",
                "I guess if you want children beaten, you have to do it yourself",
                "You know what cheers me up? Other people’s misfortune.",
                "Two oil changes for the price of one! Now if I could afford the one, and the car."};

        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        String uri = "http://futurama-service/quotes/add";

        for (int i = 11; i <= quotes.length + 10; i++) {
            Quote newQuote = new Quote();
            newQuote.setType("Success");
            Value value = new Value();
            value.setId((long) i);
            value.setQuote(quotes[i - 11]);
            newQuote.setValue(value);

            Quote returns = restTemplate.postForObject(uri, newQuote, Quote.class);
        }
    }
}