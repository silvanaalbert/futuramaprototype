package common;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class ClientApplication implements CommandLineRunner {

    public static void main(String args[]) {
        SpringApplication.run(ClientApplication.class);
    }

    @Override
    public void run(String... strings) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        int i = 1;
        while(true) {
            try {
                Quote quote = restTemplate.getForObject("http://futurama-service/quotes/random", Quote.class);
                System.out.println("Sample quote #" + i++ + ": " + quote.getValue().toString());
            } catch (Exception e) {
                System.out.println("Cannot retrieve quote at the moment");
            }
            Thread.sleep(3000);
        }
    }
}