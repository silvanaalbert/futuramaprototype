package common;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("quotes")
public class QuotesController {
    private static List<Quote> quoteList = new ArrayList<>();

    @PostConstruct
    public void setup(){
        this.buildQuotesList();
    }

    @RequestMapping("/random")
    public Quote getRandomQuote() throws UnknownHostException {
        Random rand = new Random();
        int  n = rand.nextInt(quoteList.size()-1);
        Quote quote = quoteList.get(n);
        Quote copyQuote = new Quote();
        copyQuote.setType(quote.getType());
        Value copyValue = new Value();
        copyValue.setId(quote.getValue().getId());
        copyValue.setQuote("[" + InetAddress.getLocalHost().getHostName() + "] " + quote.getValue().getQuote());
        copyQuote.setValue(copyValue);

        return copyQuote;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public Quote createQuote(@RequestBody Quote quote) {
        quoteList.add(quote);
        return quote;
    }

    private void buildQuotesList() {
        JSONParser parser = new JSONParser();
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            Object obj = parser.parse(new InputStreamReader(classLoader.getResourceAsStream("quotes.txt")));
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray quotes = (JSONArray) jsonObject.get("quotes");
            for (int i = 0; i < quotes.size(); i++) {
                JSONObject currentQuote = (JSONObject) quotes.get(i);
                JSONObject currentValue = (JSONObject) currentQuote.get("value");

                Quote quote = new Quote();
                Value value = new Value();

                quote.setType((String) currentQuote.get("type"));
                value.setId((Long) currentValue.get("id"));
                value.setQuote((String) currentValue.get("quote"));
                quote.setValue(value);

                quoteList.add(quote);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
