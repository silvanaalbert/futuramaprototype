#!/usr/bin/env bash


# start consul
docker run -d -p 8300:8300 -p 8301:8301 -p 8301:8301/udp -p 8302:8302/udp -p 8400:8400 -p 8500:8500 -p 53:53/udp  mariuslazar/consul -server -bootstrap

# start service
docker run -d -p 80:8091 mariuslazar/futurama-service

# start client
docker run -it --dns=172.17.0.1 --dns-search=service.consul mariuslazar/futurama-client
